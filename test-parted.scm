(use-modules (gnu installer newt partition)
             (gnu installer parted)
             (parted)
             (newt))

(newt-init)
(clear-screen)

(run-partioning-page))
;; (catch #t
;;   (lambda ()
;;     (run-partioning-page))
;;   (lambda _
;;     #f))

(newt-finish)
(clear-screen)
